# Music App

Ce projet a été réalisé dans le cadre d'un project d'école (Supinfo) en 2019-2020.

Le but est de créer application web lecteur audio, gérer les opérations CRUD pour les playlists et les musiques 

Une explication plus detaillée du projet se trouve dans `sujet.pdf`.


## Pré-requis
* [Visual Studio](https://visualstudio.microsoft.com/fr/)
* Installer l'outil `Développement web et ASP.NET` dans Visual Studio Installer
* Installer l'outil `Stockage et traitement des données` dans Visual Studio Installer

## Lancer le projet
1. Ouvrez le projet `music-app` avec Visual Studio
2. Puis lancez le projet


## Démo 
Vous pouvez trouvez une démo du projet via ce [lien](https://drive.google.com/file/d/12Jdq5u0n_2CtF7-2QRJs0T3i0iB6PlT2/view?usp=sharing)
