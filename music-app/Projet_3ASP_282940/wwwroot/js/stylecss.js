﻿
function SetColors(element, color) {
    for (let i of element) {
        i.style.color = color;
    }
}

function SetColor(element, color) {
    element.style.color = color;
}

function SetBackgroundColors(element, color) {
    for (let i of element) {
        i.style.backgroundColor = color;
    }
}

function SetBackgroundColor(element, color) {
    element.style.backgroundColor = color;
}

