﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Projet_3ASP_282940.Data;
using Projet_3ASP_282940.Models;

namespace Projet_3ASP_282940.Controllers
{
    public class MusicController : Controller
    {

        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment _hostingEnvironment;

        public MusicController(SignInManager<IdentityUser> signInManager, 
            ApplicationDbContext context, 
            IWebHostEnvironment hostingEnvironment)
        {
            _signInManager = signInManager;
            _context = context;
            _hostingEnvironment = hostingEnvironment;
        }

        // GET: Music
        [HttpGet]
        public ActionResult Index(int? id)
        {
            var GetPlayList = _context.PlayLists.FirstOrDefault(p => p.PlayListID == id);
            var GetAccount = _context.Accounts.FirstOrDefault(u => u.Name == User.Identity.Name);
            if (id == null ||
                GetPlayList == null ||
                !_signInManager.IsSignedIn(User) ||
                GetPlayList.UserAccount != GetAccount)
            {
                return RedirectToAction("Error");
            }
            else
            {
                try
                {
                    var GetMusics = _context.Musics.Where(m => m.MusicPlayList == GetPlayList);
                    ViewData["PlayListName"] = GetPlayList.PlayListName;
                    ViewData["PlayListID"] = GetPlayList.PlayListID;

                    CookieOptions option = new CookieOptions();
                    option.Expires = DateTime.Now.AddMinutes(10);
                    Response.Cookies.Append("PlayListIDToPlay", GetPlayList.PlayListID.ToString(), option);
                    Response.Cookies.Delete("MusicToPlay");

                    return View(GetMusics.ToList());


                    //return Ok(new { error="error" });

                }
                catch
                {
                    return RedirectToAction("Error");
                }
            }          
        }

        public ActionResult MusicToPlay(int? id)
        {
            var GetMusic = _context.Musics.FirstOrDefault(m => m.MusicID == id);
            CookieOptions option = new CookieOptions();
            option.Expires = DateTime.Now.AddMinutes(10);
            Response.Cookies.Append("MusicToPlay", GetMusic.MusicName, option);

            return RedirectToAction("Index","Player");
        }


        // GET: Music/Create
        [HttpGet]
        public ActionResult Create(int? id)
        {
            var GetPlayList = _context.PlayLists.FirstOrDefault(p => p.PlayListID == id);
            var GetAccount = _context.Accounts.FirstOrDefault(u => u.Name == User.Identity.Name);
            if (id == null ||
                GetPlayList == null ||
                !_signInManager.IsSignedIn(User) ||
                GetPlayList.UserAccount != GetAccount)
            {
                return RedirectToAction("Error");
            }
            try
            {
                ViewData["PlayListID"] = id;
                return View();
            }
            catch
            {
                return RedirectToAction("Error");
            }
            
        }

        // POST: Music/Create
        [HttpPost]
        public async Task<ActionResult> Create(List<IFormFile> files, int? id)
        {
            var GetPlayList = _context.PlayLists.FirstOrDefault(p => p.PlayListID == id);
            var GetAccount = _context.Accounts.FirstOrDefault(u => u.Name == User.Identity.Name);
            string webRootPath = _hostingEnvironment.WebRootPath;

            string name = null;
            string extension = null;
            string musicPath = null;
            long size;

            //Verify if the PlayList exist
            if (id == null ||
                GetPlayList == null ||
                !_signInManager.IsSignedIn(User) ||
                GetPlayList.UserAccount != GetAccount)
            {
                return RedirectToAction("Error");
            }

            try
            {
                ViewData["PlayListName"] = GetPlayList.PlayListName;
                ViewData["PlayListID"] = GetPlayList.PlayListID;
                ViewData["Error"] = null;
            }
            catch
            {
                return RedirectToAction("Error");
            }
               

            //Verify if the upload file is empty or not 
            if(files.Count() > 0)
            {
                name = files[0].FileName;
                extension = Path.GetExtension(name);
                musicPath = Path.Combine(webRootPath, "music", name);
                size = files[0].Length;
            }
            

            //Verify file extension and return error to the view 
            if(files.Count() <=0)
            {
                ViewData["Error"] = "Empty File";
                return View();
            }                     
            else if(extension != ".mp3")
            {
                ViewData["Error"] = "The file must be a file of type : .mp3";
                return View();
            }         
                        
            try
            {
                Music newMusic = new Music()
                {
                    MusicName = name,
                    Path = musicPath,
                    MusicPlayList = GetPlayList
                };
                _context.Musics.Add(newMusic);
                await _context.SaveChangesAsync();

                using (var fileStream = new FileStream(musicPath, FileMode.Create)) 
                await files[0].CopyToAsync(fileStream);

                return RedirectToAction("Index", "Music", new { id});
            }
            catch
            {
                return View();
            }
                     
        }


        public async Task<ActionResult> Delete (int? id, int? id2)
        {
            var GetMusic = _context.Musics.FirstOrDefault(m => m.MusicID == id2);
            var GetPlayList = _context.PlayLists.FirstOrDefault(p => p.PlayListID == id);
            var GetAccount = _context.Accounts.FirstOrDefault(u => u.Name == User.Identity.Name);

            if (id == null ||
                GetPlayList == null ||
                GetPlayList.UserAccount != GetAccount ||
                id2 == null ||
                GetMusic == null)
            {
                return RedirectToAction("Error");
            }              
            else
            {
                try
                {
                    _context.Musics.Remove(GetMusic);
                    await _context.SaveChangesAsync();

                    return RedirectToAction("Index", "Music", new { id });
                }
                catch
                {
                    return RedirectToAction("Error");
                }                
            }
        }


        public ActionResult Error()
        {
            return View();
        }
    }
}