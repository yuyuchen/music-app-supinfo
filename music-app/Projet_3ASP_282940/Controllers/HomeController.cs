﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Projet_3ASP_282940.Models;

namespace Projet_3ASP_282940.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly SignInManager<IdentityUser> _singInManager;

        public HomeController(ILogger<HomeController> logger, SignInManager<IdentityUser> sinInManager)
        {
            _logger = logger;
            _singInManager = sinInManager;
        }

        public IActionResult Index()
        {
            if (_singInManager.IsSignedIn(User))
            {
                return RedirectToAction("Index", "PlayList");
            }
            else
            {
                return View();
            }
                
        }


        public ActionResult Error()
        {
            return View();
        }

    }
}
