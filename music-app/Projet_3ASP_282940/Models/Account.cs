﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projet_3ASP_282940.Models
{
    public class Account 
    {
        public int AccountID { get; set; }
        public string Name { get; set; }
        public virtual List<PlayList> PlayLists { get; set; }

    }
}
