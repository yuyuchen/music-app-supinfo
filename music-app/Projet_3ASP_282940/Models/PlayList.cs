﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Projet_3ASP_282940.Models
{
    public class PlayList
    {
        public int PlayListID { get; set; }

        [Required]
        [Display(Name ="New Name")]
        public string PlayListName { get; set; }
        public virtual Account UserAccount { get; set; }
        public virtual List<Music> Musics { get; set; }

    }
}
