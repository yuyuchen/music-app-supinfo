﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Projet_3ASP_282940.Models
{
    public class Music
    {
        public int MusicID { get; set; }


        [Display(Name ="Music")]
        public string MusicName { get; set; }

        [Required]
        [Display(Name ="Fichier")]
        public string Path { get; set; }

        public virtual PlayList MusicPlayList { get; set; }


    }
}
